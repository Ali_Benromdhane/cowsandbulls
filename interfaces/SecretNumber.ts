interface ISecretNumber {
  removeDuplicates: (arg0: number) => number[];
}
export = ISecretNumber;
