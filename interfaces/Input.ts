interface IInput {
  suggestedNumber: () => void;
  isValidNumber: (arg0: string) => boolean;
}
export = IInput;
