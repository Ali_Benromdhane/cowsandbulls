interface IGame {
  createPlayer: (arg0: string) => Promise<void>;
  createPlayers: () => Promise<void>;
  askPlayerForNumber: (player: any) => Promise<any>;
  iterateGame: () => void;
  updateGameStatus: () => string;
  printResults: () => void;
  start: () => void;
  cowsAndBulls: (secretNumber: any, suggestedNumber: any) => string;
}
export = IGame;
