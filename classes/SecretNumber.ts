import ISecretNumber from "../interfaces/SecretNumber";

export class SecretNumber implements ISecretNumber {
  constructor() {}

  removeDuplicates() {
    let arr = [];
    while (arr.length < 4) {
      var r = Math.floor(Math.random() * 9) + 1;
      if (arr.indexOf(r) === -1) arr.push(r);
    }
    return arr;
  }
}
const test = new SecretNumber();
console.log(test.removeDuplicates());
