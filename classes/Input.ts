import IInput from "../interfaces/Input";
import prompt from "prompt";

class Input implements IInput {
  constructor() {}
  suggestedNumber() {
    var schema = {
      properties: {
        guessNumber: {
          pattern: /^(?!.*(.).*\1)\d{4}$/,
          message: "Guess Number must be only 4 Numbers distincts",
          required: true,
        },
      },
    };
    // /^[^\s]{4}$/
    prompt.get(schema, function (err, result) {
      const GuessedNumber = result.guessNumber;
      console.log("Command-line input received:");
      console.log("  Guess Number: " + GuessedNumber);
      return GuessedNumber;
    });
  }

  isValidNumber() {
    return true;
  }
}

const test = new Input();
console.log(test.suggestedNumber());
